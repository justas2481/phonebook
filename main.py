from phonebook.contactController import Menu

menu = Menu()
items = (
    ("Create new contact", menu.newContact),
    ("Add number to an existing contact", menu.addNumberToContact),
    ("Delete contact", menu.deleteContact),
    ("Contacts list", menu.contactsList),
    ("Contact preview", menu.contactPreview),
    ("Search for the contact", menu.contactSearch),
    ("Save and quit the program", menu.quit),
)
menu.load(items)
