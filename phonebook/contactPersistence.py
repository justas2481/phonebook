from phonebook.contact import Contact
import shelve
import sys
from typing import List, Tuple


class ContactPersistence:
    FILENAME: str = "contacts_db"

    def __init__(self, filename=""):
        if not filename == "":
            self.FILENAME = filename

    def getExternalContacts(self) -> Tuple[Contact, ...]:
        try:
            self.db = shelve.open(self.FILENAME)
            return tuple(self.db[key] for key in self.db)
        except:
            print(
                "For some reason there is impossible to open "
                + self.FILENAME
                + " file for the contact staurage.\nProgram terminates."
            )
            sys.exit(1)
        finally:
            self.db.close()

    def persistContacts(self, contacts: List[Contact]) -> bool:
        try:
            self.db = shelve.open(self.FILENAME)
            self.db.clear()
            for (i, contact) in enumerate(contacts):
                self.db[str(i)] = contact
        except:
            print("Problems when saving data to disk. :(")
            return False
        finally:
            self.db.close()

        return True
