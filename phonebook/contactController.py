from typing import Callable

from phonebook.contact import *
from phonebook.contactManager import *
from phonebook.contactPersistence import ContactPersistence


class Menu:
    terminate: bool = False

    def __init__(self):
        self.staurage = ContactPersistence()
        self.contactManager = ContactManager(*self.staurage.getExternalContacts())

    def load(self, items: Tuple[Tuple[str, Callable], ...]) -> None:
        while not self.terminate:
            func = self._process(items)
            func()

    def _process(self, items: Tuple[Tuple[str, Callable], ...]) -> Callable:
        def show():
            for (i, item) in enumerate(items):
                print(str(i + 1) + ".", item[0], sep=" ", end=".\n")

        show()
        itemsTotal = len(items)
        index = 0

        while True:
            try:
                index = int(input("Enter your choice:")) - 1
                if index < itemsTotal and index >= 0:
                    break
            except ValueError:
                pass
            print("Wrong input!")
            show()

        return items[index][1]

    def newContact(self):
        print("Lets make a new person known.")

        while True:
            name = input("Full name:")
            email = input("Email address (optional):")
            contactTypes = tuple(type.name for type in ContactType)
            contactType = input("Contact type ({}):".format(", ".join(contactTypes)))

            try:
                if not contactType in ContactType.__members__:
                    raise ValueError("Unknown contact type.")
            except ValueError as e:
                print(str(e))
                continue

            while True:
                try:
                    phonesTotal = abs(
                        int(
                            input(
                                "How many phones do you want to add for this contact:"
                            )
                        )
                    )
                    phonesTotal = (
                        phonesTotal if phonesTotal > 0 and phonesTotal < 101 else 1
                    )
                    break
                except ValueError:
                    print("Number of phones must be a positive number!")

            phones = []
            for i in range(phonesTotal):
                print("Enter info about phone number {}:".format(i + 1))
                phones.append(self._getPhoneData())

            try:
                self.contactManager.add(
                    Contact(name, phones, ContactType[contactType], email)
                )
                break
            except ValueError as e:
                print(str(e))

        print("Contact added successfully.")

    def contactsList(self):
        print("Existing contacts:")
        contacts = self.contactManager.getContactsForDisplay()
        if len(contacts) < 1:
            print("There are no contacts.")
            return

        self._printContacts(contacts)

    def addNumberToContact(self):
        contacts = self.contactManager.getContacts()
        index = self._getSelectedContactIndex(
            contacts, "Which contact do you want to append?"
        )
        if index < 0:
            return

        try:
            contacts[index].addPhone(self._getPhoneData())
            print("Number added.")
        except ValueError as e:
            print(str(e))

    def deleteContact(self):
        contacts = self.contactManager.getContacts()
        index = self._getSelectedContactIndex(
            contacts, "Which contact do you want to delete?"
        )
        if index < 0:
            return

        self.contactManager.delete(index)
        print("Contact deleted.")

    def contactSearch(self):
        keyword = input("Enter a contact name you want to find:")
        indexes = self.contactManager.searchByName(keyword)
        if len(indexes) < 1:
            print('No search results by keyword "{}".'.format(keyword))
            return

        self._printContacts(self.contactManager.getContactsForDisplay(indexes))

    def contactPreview(self):
        contacts = self.contactManager.getContacts()
        if len(contacts) < 1:
            print("There are no contacts.")
            return

        index = self._getSelectedContactIndex(
            contacts, "Which contact you want to preview?"
        )
        contact = self.contactManager.getContactForDisplay(index)
        print("Contact information:")

        for key in contact:
            if key == "Phones":
                for (i, phone) in enumerate(contact[key]):
                    print("Phone number ", i + 1, ":", sep="")
                    for phoneKey in phone:
                        print(phoneKey + ": ", phone[phoneKey])
            else:
                print(key + ": " + contact[key])

    def _getPhoneData(self) -> Phone:
        phoneTypes = tuple(type.name for type in PhoneType)

        while True:
            phoneType = input("Phone type ({}):".format(", ".join(phoneTypes)))
            phoneNumber = input(
                "Phone number (+(country code)(number) i.e. +370********):"
            )
            try:
                if not phoneType in PhoneType.__members__:
                    raise ValueError("Unknown phone type.")
                return Phone(phoneNumber, PhoneType[phoneType])
            except ValueError as e:
                print(str(e))

    def _getSelectedContactIndex(self, contacts: List[Contact], msg: str = "") -> int:
        if len(contacts) < 1:
            print("There are no contacts.")
            return -1

        for (i, contact) in enumerate(contacts):
            print("{}. {} ({})".format(i + 1, contact.name, contact.email))

        while True:
            try:
                index = abs(int(input(msg)))
                if index < 1 or index > len(contacts):
                    raise ValueError()
                index -= 1
                return index
            except ValueError:
                print("Incorrect choice!")

    def _printContacts(self, contacts: List[Dict]) -> None:
        for (i, contact) in enumerate(contacts):
            phones = ", ".join(
                [
                    str(phone["Country"] + " " + phone["Phone number"])
                    for phone in contact["Phones"]
                ]
            )
            print(
                "{}. {} ({})".format(i + 1, contact["Firstname"], phones), end=".\n\n"
            )

    def quit(self):
        self.staurage.persistContacts(self.contactManager.getContacts())
        self.terminate = True
        print("Bye bye.")
