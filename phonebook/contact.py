from typing import List
from enum import Enum
from datetime import datetime
import re


class PhoneType(Enum):
    home = 0
    work = 1
    other = 2


class ContactType(Enum):
    friend = 0
    familyMember = 1
    colleague = 2
    other = 3


class Phone:
    def __init__(self, number: str, type: PhoneType):
        self.number = number
        self.type = type

    @property
    def number(self) -> str:
        return self._number

    @number.setter
    def number(self, value: str) -> None:
        if len(value) > 12:
            raise ValueError("Phone number is to long.")
        if len(value) < 11:
            raise ValueError("Phone number is to short.")
        if not value.startswith("+") or not value[1:].isdigit():
            raise ValueError("Phone number is invalid.")
        self._number = value

    @property
    def type(self):
        return self._type.name

    @type.setter
    def type(self, value: PhoneType):
        self._type = value

    def getCountry(self) -> str:
        countries = {"lt": "+370", "uk": "+44"}
        for country in countries:
            if self.number.startswith(countries[country]):
                break
        else:
            country = "Unknown"
        return country


class Contact:
    def __init__(
        self,
        name: str,
        phones: List[Phone],
        type: ContactType,
        email: str = "",
    ) -> None:
        self.name = name
        self.phones = phones
        self.email = email
        self.type = type
        self.dateCreated = datetime.now()

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str) -> None:
        if len(value) > 20:
            raise ValueError("Name is to long.")
        if len(value) < 5:
            raise ValueError("Name is to short.")
        if not " " in value:
            raise ValueError(
                "Name is incorrect. It has to consist of firstname and lastname, separated by space."
            )
        self._name = value
        firstname = self.getFirstname()
        lastname = self.getLastname()
        if len(firstname) < 2:
            raise ValueError("Firstname is to short.")
        if len(lastname) < 2:
            raise ValueError("Lastname is to short.")
        regex = "[a-zA-Z]"
        if not re.match(regex, firstname) or not re.match(regex, lastname):
            raise ValueError(
                "Firstname and lastname has to have at least 1 alphabet (a-z A-Z) character."
            )

    def getFirstname(self) -> str:
        return self.name.split(" ")[0]

    def getLastname(self) -> str:
        return self.name.split(" ")[-1]

    @property
    def phones(self) -> List[Phone]:
        return self._phones

    @phones.setter
    def phones(self, value: List[Phone]) -> None:
        if len(value) != len(set(value)):
            raise ValueError("Phones must be unique per contact.")
        self._phones = value

    def addPhone(self, phone: Phone) -> None:
        if phone.number in self.getPhoneNumbers():
            raise ValueError("This number is not unique per contact.")
        self.phones.append(phone)

    @property
    def email(self) -> str:
        if len(self._email) == 0:
            return "n/a"
        return self._email

    @email.setter
    def email(self, value: str) -> None:
        if value != "" and (
            not re.match("^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$", value)
            or len(value) > 70
        ):
            raise ValueError("Email address is not valid.")
        self._email = value

    @property
    def type(self):
        return self._type.name

    @type.setter
    def type(self, value: ContactType):
        self._type = value

    def getDateCreated(self) -> str:
        return datetime.strftime(self.dateCreated, "%Y-%M-%d")

    def getPhoneNumbers(self) -> List[str]:
        return [num.number for num in self.phones]
