from typing import Dict, List, Tuple, Union

from phonebook.contact import Contact, ContactType, Phone, PhoneType


class ContactManager:
    def __init__(self, *contacts: Contact):
        self.contacts: List[Contact] = []

        if len(contacts) > 0:
            self._arePhonesUnique(*contacts)
            self._areEmailsUnique(*contacts)

        self.contacts = list(contacts)
        self.sort()

    def add(self, contact: Contact) -> None:
        self._arePhonesUnique(contact)
        self._areEmailsUnique(contact)
        self.contacts.append(contact)
        self.sort()

    def delete(self, index: int) -> Union[Contact, bool]:
        index = abs(index)

        if not self.contacts == [] and len(self.contacts) > index:
            return self.contacts.pop(index)

        return False

    def getContacts(self) -> List[Contact]:
        return self.contacts

    def getContactsForDisplay(self, indexes: Tuple[int, ...] = ()) -> List[Dict]:
        contacts = []
        onlyIndexedContacts = len(indexes) > 0

        for (i, contact) in enumerate(self.contacts):
            if onlyIndexedContacts and not i in indexes:
                continue

            contactPhones = []

            for phone in contact.phones:
                contactPhone = dict()
                contactPhone["Phone type"] = phone.type
                contactPhone["Country"] = phone.getCountry()
                contactPhone["Phone number"] = phone.number
                contactPhones.append(contactPhone)

            data = {
                "Name": contact.name,
                "Firstname": contact.getFirstname(),
                "Lastname": contact.getLastname(),
                "Phones": contactPhones,
                "Email": contact.email,
                "Contact type": contact.type,
                "Date created": contact.getDateCreated(),
            }
            contacts.append(data)

        return contacts

    def getContactForDisplay(self, index: int) -> Union[Dict, bool]:
        index = abs(index)

        if len(self.contacts) > index:
            data = self.getContactsForDisplay()
            if not data == []:
                return data[index]

        return False

    def searchByName(self, name: str) -> Tuple[int, ...]:
        return tuple(
            [
                index
                for (index, contact) in enumerate(self.contacts)
                if contact.name.__contains__(name)
            ]
        )

    def _arePhonesUnique(self, *contacts: Contact) -> None:
        newNumbers = [
            number for contact in contacts for number in contact.getPhoneNumbers()
        ]

        if len(self.contacts) > 0:
            existingNumbers = [
                number
                for contact in self.contacts
                for number in contact.getPhoneNumbers()
            ]

            for number in newNumbers:
                if number in existingNumbers:
                    raise ValueError("Phone numbers must be unique per contact.")

        if len(newNumbers) != len(set(newNumbers)):
            raise ValueError(
                "Per same contact numbers must be unique, there is no point to have duplicates."
            )

    def _areEmailsUnique(self, *contacts: Contact) -> None:
        emails = (contact.email for contact in self.contacts)

        for contact in contacts:
            if contact.email in emails:
                raise ValueError("Emails must be unique to each contact.")

    def sort(self) -> None:
        self.contacts = sorted(self.contacts, key=(lambda contact: contact.name))
