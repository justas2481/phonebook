# Phonebook

* User can have many contacts in his Phonebook.

## Contact

1. Full name (first name + last name) separated by spaces ' '. If more than 1 space is presented, consider firstname to be the first word before the first space, and lastname is the last word (after the last space). Validation: Full name can not be shorter than 5 and be longer than 20 characters and at least one space must be presented somewhere in the middle.
1. Firstname (extracted from full name). Validation: can not be shorter than 2 characters and at least 1 alphabet letter [a-z or A-Z] must be presented.
1. Lastname (extracted from full name). Validation: can not be shorter than 2 characters and and at least 1 alphabet letter [a-z or A-Z] must be presented.
1. Phone (can be many phones per contact), described further.
1. Contact type (friend, family member, colleague, other).
1. Email address. Validation: should be some alike real email address and validated with regex. Email should not be required, but must be unique per contact.
1. Date created (year-month-day (of the moment when contact was created)).

## Phone

1. Phone type (home, work, other).
1. Phone number. Validation: it can consist of 11 or 12 characters only and has to have '+' character in front, everything else must be digits.
1. Country (should be determined by the phone number, if for example phone number starts with '+370', then country = 'lt', if it's '+44', then country = 'uk', if other country is presented, country = 'unknown'). List of countries should be easily appended if needed.
1. Phone must be unique per contact and all contacts must have unique numbers. I.e. same number can belong only to one contact in particular.

## User

1. Create new contact by filling information about the contact (including phones with their corresponding info such as Phone type and Phone number). Be ware, that one contact might have more than one number, i.e. home number, work number and these numbers might be from different countries.
1. Ad additional number to an existing contact (this functionality should be reachable from contact preview (described later on).
1. Delete contact by it's index.
1. See contacts list with their corresponding information. Feel free add what you like here, but at least contact index, Firstname, and contact's phones (with information which country the numbers belong to) must be presented.
1. Preview contact. Contact is fetch by it's index. Here you should display all the information is known about the contact including, but not limited to Name, Contact type, Email, Phones etc.
1. Search for contact by it's name (can be combination of firstname + lastname). If user enters only a partial name, most similar results should be fetched and displayed the same way as in the 5th action in the menu (just displayed the exact way as in contacts list except that only found items shall be shown in this case).

## Additional requirements

* Validation should ask user to reenter the values when error is described, but should not break the program if user enters non-sense.
* Contacts should be persisted to the text file, Python Pickle or Shelve, SQL database, Mongo DB, CSV, JSON, or XML file, it is for you to choose most appropriate.